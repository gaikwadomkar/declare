﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class swipe2 : MonoBehaviour {

    public GameObject img;
    //xpublic IMage image;
    public Sprite[] imgs;
    public float angleOfSwipe;
    int counter = 0;
    const int mMessageWidth = 200;
    const int mMessageHeight = 64;

    readonly Vector2 mXAxis = new Vector2(1, 0);
    readonly Vector2 mYAxis = new Vector2(0, 1);

    readonly string[] mMessage = {
       "",
       "Swipe Left",
       "Swipe Right",
       "Swipe Top",
       "Swipe Bottom"
    };

    int mMessageIndex = 0;

    // The angle range for detecting swipe
    const float mAngleRange = 30;

    // To recognize as swipe user should at lease swipe for this many pixels
    const float mMinSwipeDist = 5.0f;

    // To recognize as a swipe the velocity of the swipe
    // should be at least mMinVelocity
    // Reduce or increase to control the swipe speed
    const float mMinVelocity = 500.0f;

    Vector2 mStartPosition;
    float mSwipeStartTime;
    // Use this for initialization
    void Start()
    {
        if (counter == 0)
        {
            Debug.Log("this is first sprite");
        }
    }

    // Update is called once per frame
    void Update()
    {

        // Mouse button down, possible chance for a swipe
        if (Input.GetMouseButtonDown(0))
        {
            // Record start time and position
            mStartPosition = new Vector2(Input.mousePosition.x,
                                         Input.mousePosition.y);
            mSwipeStartTime = Time.time;
        }

        // Mouse button up, possible chance for a swipe
        if (Input.GetMouseButtonUp(0))
        {
            float deltaTime = Time.time - mSwipeStartTime;

            Vector2 endPosition = new Vector2(Input.mousePosition.x,
                                               Input.mousePosition.y);
            Vector2 swipeVector = endPosition - mStartPosition;

            float velocity = swipeVector.magnitude / deltaTime;

            if (velocity > mMinVelocity &&
                swipeVector.magnitude > mMinSwipeDist)
            {
                // if the swipe has enough velocity and enough distance

                swipeVector.Normalize();

                angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                // Detect left and right swipe
                if (angleOfSwipe < mAngleRange)
                {
                    OnSwipeRight();

                }

                else if ((180.0f - angleOfSwipe) < mAngleRange)
                {
                    OnSwipeLeft();
                }
                else
                {
                }
            }
        }
    }

    void OnGUI()
    {
    }

    void OnSwipeLeft()
    {
        Debug.Log("swipe left");


        mMessageIndex = 1;

        if (counter == 6)
        {
            return;
        }
        counter++;
        img.GetComponent<Image>().sprite = imgs[counter];
    }

    void OnSwipeRight()
    {
        Debug.Log("swipe right");
        mMessageIndex = 2;
        if (counter == 0)
        {
            SceneManager.LoadScene("Page1");
            return;
        }
        counter--;
        img.GetComponent<Image>().sprite = imgs[counter];


    }
    void OnSwipeRightScene()
    {

    }
}
